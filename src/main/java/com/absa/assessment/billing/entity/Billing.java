package com.absa.assessment.billing.entity;

import com.absa.assessment.billing.model.BillingSummary;
import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import java.util.Date;

@SqlResultSetMapping(name = "billingSummary", classes = @ConstructorResult(
        targetClass = com.absa.assessment.billing.model.BillingSummary.class,
        columns = {
                @ColumnResult(name = "client_swift_address"),
                @ColumnResult(name = "usageStats", type = Integer.class),
                @ColumnResult(name = "subService")
        }
))

@NamedNativeQuery(name = "retrieveAllBilling", query = "select b.client_swift_address, count(b.client_swift_address) as usageStats, 'CCYOUT' as subService from billing b " +
        "where b.currency <> 'ZAR' and b.message_status = 'complete'" +
        "group by b.client_swift_address , b.date_time_created " +
        "having date_part('month', current_date) - date_part('month', b.date_time_created) = 1 " +
        "UNION ALL " +
        "select b.client_swift_address, count(b.client_swift_address) as usageStats, replace(b.currency, 'ZAR', 'ZAROUT') as subService " +
        "from Billing b " +
        "where b.currency = 'ZAR' and b.message_status = 'complete' " +
        "group by b.client_swift_address, b.date_time_created, b.currency " +
        "having date_part('month', current_date) - date_part('month', b.date_time_created) = 1",
        resultClass = BillingSummary.class, resultSetMapping = "billingSummary")

@Entity
public class Billing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String transactionReference;
    private String clientSwiftAddress;
    private String messageStatus;
    private String currency;
    private double amount;
    private java.util.Date dateTimeCreated;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getClientSwiftAddress() {
        return clientSwiftAddress;
    }

    public void setClientSwiftAddress(String clientSwiftAddress) {
        this.clientSwiftAddress = clientSwiftAddress;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setDateTimeCreated(Date dateTimeCreated) {
        this.dateTimeCreated = dateTimeCreated;
    }
}