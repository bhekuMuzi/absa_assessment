package com.absa.assessment.billing.repository;

import com.absa.assessment.billing.entity.BillingSummaryHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillingHistoryRepository extends JpaRepository<BillingSummaryHistory, Integer> {

}