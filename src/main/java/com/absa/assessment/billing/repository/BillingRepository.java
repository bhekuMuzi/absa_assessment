package com.absa.assessment.billing.repository;

import com.absa.assessment.billing.entity.Billing;
import com.absa.assessment.billing.model.BillingSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BillingRepository extends JpaRepository<Billing, Integer> {

    @Query(nativeQuery = true, name = "retrieveAllBilling")
    List<BillingSummary> retrieveAllBilling();
}