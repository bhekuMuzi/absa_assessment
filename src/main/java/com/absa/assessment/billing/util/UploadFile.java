package com.absa.assessment.billing.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import java.nio.file.Files;

public class UploadFile {

    public String uploadFileToSharePoint()  {
        String realPath = System.getProperty("user.dir");
        String filePath = realPath + "/src/main/resources/file/billing-summary.txt";
        String copyPath = realPath + "/src/main/webapps/uploaded-files/billing-summary.txt";
        File file = new File(filePath);

        Path path = Paths.get(copyPath);
        Path originalPath = file.toPath();

        if (!Files.exists(originalPath)){
            return "File hasn't been created yet";
        }

        try {
            Files.copy(originalPath, path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "File Uploaded.";
    }
}