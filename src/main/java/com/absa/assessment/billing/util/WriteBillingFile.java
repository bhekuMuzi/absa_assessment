package com.absa.assessment.billing.util;

import com.absa.assessment.billing.model.BillingSummary;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WriteBillingFile {

    public static void writeToFile(String fileName, BillingSummary billing) {
        String realPath = System.getProperty("user.dir");
        String path = realPath + "/src/main/resources/file/" + fileName + ".txt";
        String fileLine = createFileLine(billing);
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(path, true))) {
            printWriter.println(fileLine);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String createFileLine(BillingSummary billingSummary) {
        String fileLine = "";
        fileLine += "INTEGRATEDSERVICES" + billingSummary.getClientSwiftAddress() + billingSummary.getSubService() + formatDate(new java.util.Date()) + billingSummary.getUsageStats();
        return fileLine;
    }

    private static String formatDate(Date dateTimeCreated) {
        return new SimpleDateFormat("YYYYMMdd").format(dateTimeCreated);
    }
}