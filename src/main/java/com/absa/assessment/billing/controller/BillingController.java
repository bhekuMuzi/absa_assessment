package com.absa.assessment.billing.controller;

import com.absa.assessment.billing.util.UploadFile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("billing")
public class BillingController{

    @RequestMapping(value = {"", "upload"}, method = RequestMethod.GET)
    public String uploadBillingSummary (){
        UploadFile uploadFile = new UploadFile();
        return uploadFile.uploadFileToSharePoint();
    }
}