package com.absa.assessment.billing;

import java.util.Arrays;
import java.util.List;

public class JavaStreams {

    public static void main(String[] args) {
        List<String> statuses = Arrays.asList("Received", "Pending", "", "Awaiting Maturity",
                "Completed", "");

        // stream() without specifying the type defaults to sequential which uses one thread
        statuses.stream().forEach(s -> {
            if(!s.isEmpty()) {
                System.out.println("status : " + s + " thread : " + Thread.currentThread().getName());
            }
        });

        System.out.println();

        // you can still specify you want a sequential stream steam().sequential()
        statuses.stream().forEach(s -> {
            if(!s.isEmpty()) {
                System.out.println("status : " + s + " thread : " + Thread.currentThread().getName());
            }
        });

        System.out.println();

        // stream().parallel() breaks down the task and runs them in using different threads
        statuses.stream().parallel().forEach(s -> {
            if(!s.isEmpty()) {
                System.out.println("status : " + s + " thread : " + Thread.currentThread().getName());
            }
        });

    }
}