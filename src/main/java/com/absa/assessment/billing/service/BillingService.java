package com.absa.assessment.billing.service;

import com.absa.assessment.billing.entity.BillingSummaryHistory;
import com.absa.assessment.billing.model.BillingSummary;
import com.absa.assessment.billing.repository.BillingHistoryRepository;
import com.absa.assessment.billing.repository.BillingRepository;
import com.absa.assessment.billing.util.WriteBillingFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BillingService {

    private BillingRepository billingRepository;
    private BillingHistoryRepository billingHistoryRepository;

    @Autowired
    public BillingService(BillingRepository billingRepository, BillingHistoryRepository billingHistoryRepository) {
        this.billingRepository = billingRepository;
        this.billingHistoryRepository = billingHistoryRepository;
    }

    public String retrieveBillingFillingInfo() {
        List<BillingSummary> billings = billingRepository.retrieveAllBilling();

        billings.stream().forEach(billing ->
                WriteBillingFile.writeToFile("billing-summary", billing)
        );
        StringBuilder fileContent = new StringBuilder();

        billings.stream().forEach(billingSummary -> fileContent.append(billingSummary.getClientSwiftAddress() + "\t" + "INTEGRATEDSERVICES" + "\t" +billingSummary.getSubService() + "\t" + formatDate(new java.util.Date()) + "\t" + billingSummary.getUsageStats()));

        billingHistoryRepository.save(new BillingSummaryHistory(new java.util.Date(), fileContent.toString()));
        return "File created";
    }

    private String formatDate(Date date) {
        return new SimpleDateFormat("YYYYMMdd").format(date);
    }
}