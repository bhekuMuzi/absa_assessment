package com.absa.assessment.billing.component;

import com.absa.assessment.billing.service.BillingService;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.config.ScheduledTask;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
@ShellComponent
@NoArgsConstructor
public class BillingComponent {

    private BillingService billingService;

    @Autowired
     public BillingComponent(BillingService billingService){
         this.billingService = billingService;
     }

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTask.class);
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Scheduled(cron = "0 30 7 1 * ?")
    @ShellMethod(value = "create file", key = "billingFile")
    public String loadBillingInfo(){
        String retrieveBillingFillingInfo = billingService.retrieveBillingFillingInfo();
        logger.info("{} :: Day executed - {} ", retrieveBillingFillingInfo, dateFormatter.format(LocalDateTime.now()));
        return retrieveBillingFillingInfo;
    }
}