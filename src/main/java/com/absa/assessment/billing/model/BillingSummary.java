package com.absa.assessment.billing.model;

public class BillingSummary {

    private String clientSwiftAddress;
    private Integer usageStats;
    private String subService;

    public BillingSummary(String clientSwiftAddress, Integer usageStats, String subService) {
        this.clientSwiftAddress = clientSwiftAddress;
        this.usageStats = usageStats;
        this.subService = subService;
    }

    public BillingSummary(){}

    public String getClientSwiftAddress() {
        return clientSwiftAddress;
    }

    public void setClientSwiftAddress(String clientSwiftAddress) {
        this.clientSwiftAddress = clientSwiftAddress;
    }

    public String getSubService() {
        return subService;
    }

    public void setSubService(String subService) {
        this.subService = subService;
    }

    public Integer getUsageStats() {
        return usageStats;
    }

    public void setUsageStats(Integer usageStats) {
        this.usageStats = usageStats;
    }
}