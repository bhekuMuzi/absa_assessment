create table billing_summary_history(
    id serial primary key,
    date_created timestamp,
    file_content varchar(1000)
);