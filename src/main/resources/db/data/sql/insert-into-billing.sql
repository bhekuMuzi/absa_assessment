insert into billing (transaction_reference, client_swift_address, message_status, currency, amount, date_time_created)
values ('abc-reference', 'CETYUS66', 'complete', 'USD', 5000, '2019-11-5'), ('def-reference', 'ECATZAJ4', 'successful', 'ZAR', 2500, '2019-11-29'),
('abc-reference', 'AAXXZAJJCBF', 'complete', 'ZAR', 1000, '2019-11-10'), ('def-reference', 'PMFAUS66HKG', 'in progess', 'XCD', 5000, '2019-11-5'),
('abc-reference', 'MMMCUS44', 'complete', 'EUR', 5000, '2019-11-5'), ('abc-reference', 'CETYUS66', 'complete', 'ZAR', 5000, '2019-11-5'), ('def-reference', 'ECATZAJ4', 'successful', 'ZAR', 2500, '2019-7-29'),
('abc-reference', 'AAXXZAJJCBF', 'complete', 'ZAR', 1000, '2019-9-10'), ('def-reference', 'PMFAUS66HKG', 'in progess', 'XCD', 5000, '2019-8-5'),
('abc-reference', 'MMMCUS44', 'complete', 'ZAR', 5000, '2019-11-5'), ('abc-reference', 'CETYUS66', 'complete', 'GPD', 5000, '2019-11-5'), ('def-reference', 'ECATZAJ4', 'successful', 'AFN', 2500, '2019-11-29'),
('abc-reference', 'AAXXZAJJCBF', 'complete', 'EUR', 1000, '2019-11-10');