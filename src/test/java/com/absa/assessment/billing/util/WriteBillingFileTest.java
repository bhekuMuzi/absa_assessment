package com.absa.assessment.billing.util;

import com.absa.assessment.billing.model.BillingSummary;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;

public class WriteBillingFileTest {

    @Test
    public void shouldCreateFile(){
        BillingSummary billingSummary = new BillingSummary("AAAAAA", 2, "CCYOUT");
        WriteBillingFile.writeToFile("billing-summary-test", billingSummary);

        String root = System.getProperty("user.dir");
        String createdPath = root + "/src/main/resources/file/billing-summary-test.txt";
        File createdFile = new File(createdPath);

        Path createdFilePath = createdFile.toPath();

        Assertions.assertThat(createdFilePath).exists();
    }
}