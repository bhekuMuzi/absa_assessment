package com.absa.assessment.billing.util;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.Files;

import static org.assertj.core.api.Assertions.assertThat;

public class UploadFileTest {

    @Test
    public void shouldUploadFileToWebappsWhenInvoked(){
        UploadFile uploadFile = new UploadFile();
        uploadFile.uploadFileToSharePoint();

        String realPath = System.getProperty("user.dir");
        String copyPath = realPath + "/src/main/webapps/uploaded-files/billing-summary.txt";

        Path copied = Paths.get(copyPath);

        assertThat(copied).exists();
    }

    @Test
    public void uploadedFileShouldHaveTheSameContentAsCreatedFile() throws IOException {
        String realPath = System.getProperty("user.dir");
        String filePath = realPath + "/src/main/resources/file/billing-summary.txt";
        String copyPath = realPath + "/src/main/webapps/uploaded-files/billing-summary.txt";
        File file = new File(filePath);

        Path copied = Paths.get(copyPath);
        Path originalPath = file.toPath();

        Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);

        assertThat(Files.readAllLines(originalPath)
                .equals(Files.readAllLines(copied)));
    }

}