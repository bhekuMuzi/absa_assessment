package com.absa.assessment.billing.service;

import com.absa.assessment.billing.model.BillingSummary;
import com.absa.assessment.billing.repository.BillingHistoryRepository;
import com.absa.assessment.billing.repository.BillingRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.is;

public class BillingServiceTest {

    @Test
    public void shouldNotCreateFile() {
        BillingRepository billingRepository = Mockito.mock(BillingRepository.class);
        BillingHistoryRepository billingHistoryRepository = Mockito.mock(BillingHistoryRepository.class);
        BillingService billingService = new BillingService(billingRepository, billingHistoryRepository);
        List<BillingSummary> expected = Collections.emptyList();
        Mockito.when(billingRepository.retrieveAllBilling()).thenReturn(expected);

        String actual = billingService.retrieveBillingFillingInfo();

        Assert.assertThat(actual, is("File created"));
    }
}