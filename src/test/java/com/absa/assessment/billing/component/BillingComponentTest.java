package com.absa.assessment.billing.component;

import org.junit.Test;

import java.time.Duration;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class BillingComponentTest {

    @Test
    public void shouldNotInvokeMethodIfTheDateHasntArrived() {
        BillingComponent billingComponent = mock(BillingComponent.class);
        await().atMost(Duration.ofSeconds(100)).
                untilAsserted(() ->
                        verify(billingComponent, times(0)).loadBillingInfo());
    }
}